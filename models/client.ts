import mongoose from 'mongoose'

export interface ClientInterface extends mongoose.Document {
  name: String,
  date_install: Date,
  date_buy: Date,
  email: { type: String, required: true },
  is_partner: Boolean,
  mail_demo: Boolean,
  mail_buy: Boolean,
  mpx_key: String,
  reviewed: Boolean
}

const ClientSchema = new mongoose.Schema({
  name: String,
  date_install: Date,
  date_buy: {
    type: Date,
    default: null
  },
  email: {type: String, required: true},
  is_partner: Boolean,
  mail_demo: Boolean,
  mail_buy: Boolean,
  mpx_key: String,
  reviewed: Boolean
})

export const Client = mongoose.model<ClientInterface>('Client', ClientSchema)
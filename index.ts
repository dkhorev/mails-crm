import express from 'express'
import mongoose from 'mongoose'
import bodyParser from 'body-parser'
import session from 'express-session'
import connectFlash from 'connect-flash'

// const expressEdge = require('express-edge')
const { config, engine } = require('express-edge');
const ClientController = require('./controllers/ClientController')

const app = express()
const clientController = new ClientController()

const dbHost = process.env.DB_HOST || 'mongodb://localhost/mail-crm-dev'
mongoose.connect(dbHost, {useNewUrlParser: true}, (error) => {
  if (!error) {
    console.log(`DB connected ${dbHost}`)
  } else {
    console.log(error)
  }
})

// bodyparser
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))

// flash messages
app.use(session({
  secret: 'secret',
  resave: false,
  saveUninitialized: true
}))
app.use(connectFlash())

// static assets setup
app.use(express.static('public'))

// Automatically sets view engine and adds dot notation to app.render
app.use(engine)
app.set('views', `${__dirname}/views`)

// Routing
app.get('/', clientController.index)
app.get('/client/create', clientController.create)
app.post('/client/create', clientController.store)
app.get('/client/show/:id', clientController.show)
app.post('/client/show/:id', clientController.update)
app.get('/client/mail-demo', clientController.indexDemo)
app.get('/client/mail-demo/done/:id', clientController.mailDoneDemo)
app.get('/client/mail-buy', clientController.indexBuy)
app.get('/client/mail-buy/done/:id', clientController.mailDoneBuy)

// 404
app.use((req, res) => res.render('404'))

const appPort = process.env.PORT || 3000
app.listen(appPort, () => {
  console.log(`Listening on port ${appPort}`)
})

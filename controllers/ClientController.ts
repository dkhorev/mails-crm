import {Request, Response} from 'express'

import {Client, ClientInterface} from '../models/Client'

// days for long timers comparison
const LONG_DEMO: number = 45 + 7
const LONG_BUY: number = 7

class ClientController {

  getCounters = (clients: Array<any>) => {
    // count by types
    const countAll = clients.length

    const countBuy = clients.filter((client: ClientInterface) => {
      return !!client.date_buy
    }).length

    const countReviewed = clients.filter((client: ClientInterface) => {
      return client.reviewed
    }).length

    // count demo long
    const today = new Date()
    const countDemoLong = clients.filter((client: ClientInterface) => {
      const compareDate = new Date(client.date_install)
      compareDate.setDate(client.date_install.getDate() + LONG_DEMO)

      return compareDate <= today && !client.date_buy && !client.mail_demo
    }).length

    // count buy long
    const countBuyLong = clients.filter((client: ClientInterface) => {
      if (client.date_buy) {
        const compareDate = new Date(client.date_buy)
        compareDate.setDate(client.date_buy.getDate() + LONG_BUY)

        return compareDate <= today && !client.mail_buy
      }

      return false
    }).length

    return {
      countAll, countBuy, countDemoLong, countBuyLong, countReviewed
    }
  }

  /**
   * Get all clients
   *
   * @param {*} req
   * @param {*} res
   * @returns
   */
  index = async (req: Request, res: Response) => {
    /** @type Array clients */
    const clients: Array<any> = await Client.find({}, null, {sort: {date_install: 'desc'}}, (err: Object) => {
      if (err) {
        console.log(err)
      }
    })

    return res.render('index', {clients, ...this.getCounters(clients)})
  }

  /**
   * Get all clients which must receive mails about demo period
   *
   * @param {*} req
   * @param {*} res
   * @returns
   */
  indexDemo = async (req: Request, res: Response) => {
    /** @type Array clients */
    let clients: Array<any> = await Client.find({}, null, {sort: {date_install: 'asc'}}, (err: Object) => {
      if (err) {
        console.log(err)
      }
    })

    const counters = this.getCounters(clients)

    // filter clients by date install
    const today = new Date()
    clients = clients.filter((client: ClientInterface) => {
      const compareDate = new Date(client.date_install)
      compareDate.setDate(client.date_install.getDate() + LONG_DEMO)

      return compareDate <= today && !client.date_buy && !client.mail_demo
    })

    return res.render('index-mail-demo', {clients, ...counters})
  }

  /**
   * Mark demo mail as sent and return to mail demo index page
   *
   * @param {*} req
   * @param {*} res
   * @returns
   */
  mailDoneDemo = async (req: Request, res: Response) => {
    const id = req.params.id

    // @ts-ignore
    await Client.findByIdAndUpdate(id, {mail_demo: true}, (err: Object) => {
      if (err) {
        return res.redirect('/client/mail-demo')
      }
    })

    return res.redirect('/client/mail-demo')
  }

  /**
   * Get all clients which must receive mails for review
   *
   * @param {*} req
   * @param {*} res
   * @returns
   */
  indexBuy = async (req: Request, res: Response) => {
    /** @type Array clients */
    let clients: Array<any> = await Client.find({}, null, {sort: {date_buy: 'asc'}}, (err: Object) => {
      if (err) {
        console.log(err)
      }
    })

    const counters = this.getCounters(clients)

    // filter clients by date buy
    const today = new Date()
    // only guys who bought it
    clients = clients.filter((client: ClientInterface) => {
      return !!client.date_buy && !client.mail_buy
    })
    clients = clients.filter((client: ClientInterface) => {
      const compareDate = new Date(client.date_buy)
      compareDate.setDate(client.date_buy.getDate() + LONG_BUY)

      return compareDate <= today
    })

    return res.render('index-mail-buy', {clients, ...counters})
  }

  /**
   * Mark buy mail as sent and return to mail demo index page
   *
   * @param {*} req
   * @param {*} res
   * @returns
   */
  mailDoneBuy = async (req: Request, res: Response) => {
    const id = req.params.id

    // @ts-ignore
    await Client.findByIdAndUpdate(id, {mail_buy: true}, (err: Object) => {
      if (err) {
        return res.redirect('/client/mail-buy')
      }
    })

    return res.redirect('/client/mail-buy')
  }

  /**
   * Show create new client form
   *
   * @param {*} req
   * @param {*} res
   * @returns
   */
  create = (req: Request, res: Response) => {
    const data = {
      errors: req.flash('storeErrors').pop(),
      postData: req.flash('postData').pop()
    };

    return res.render('client/create', data)
  }

  /**
   * Save new client data
   *
   * @param {*} req
   * @param {*} res
   * @returns
   */
  store = async (req: Request, res: Response) => {
    const params = req.body
    params.is_partner = params.is_partner === 'on'
    params.mail_demo = params.mail_demo === 'on'
    params.mail_buy = params.mail_buy === 'on'
    params.reviewed = params.reviewed === 'on'

    // validate if duplicate exists
    // @ts-ignore
    Client.findOne({email: params.email}, async (err, user) => {
      if (user) {
        req.flash('storeErrors', `Такой емейл уже есть в базе: ${params.email}`)
        req.flash('postData', req.body)

        return res.redirect('/client/create')
      } else {
        await Client.create(params, (err: Object) => {
          if (err) {
            console.log(err)
            return res.redirect('/client/create')
          }
        })

        return res.redirect('/')
      }
    })

  }

  /**
   * Show single client card
   *
   * @param {*} req
   * @param {*} res
   * @returns
   */
  show = async (req: Request, res: Response) => {
    const id = req.params.id

    /** @type ClientInterface client */
    const client = await Client.findById(id, (err: Object) => {
      if (err) {
        console.log(err)
        return res.redirect('/')
      }
    })

    if (client) {
      const result = JSON.parse(JSON.stringify(client))
      result.date_buy = client.date_buy ? client.date_buy.toISOString().split('T')[0] : '-'
      result.date_install = client.date_install ? client.date_install.toISOString().split('T')[0] : '-'

      return res.render('client/create', {result})
    }

    return res.redirect('/')
  }

  /**
   * Update single client
   *
   * @param {*} req
   * @param {*} res
   */
  update = async (req: Request, res: Response) => {
    const id = req.params.id
    const params = req.body
    params.is_partner = params.is_partner === 'on'
    params.mail_demo = params.mail_demo === 'on'
    params.mail_buy = params.mail_buy === 'on'
    params.reviewed = params.reviewed === 'on'

    // validate if duplicate exists
    // @ts-ignore
    Client.findOne({email: params.email}, async (err, user) => {

      if (user && id !== user._id.toString()) {
        req.flash('storeErrors', `Такой емейл уже есть в базе: ${params.email}`)
        req.flash('postData', req.body)

        res.redirect(`/client/show/${id}`)
      } else {
        // @ts-ignore
        await Client.findByIdAndUpdate(id, params, (err: Object) => {
          if (err) {
            return res.redirect(`/client/show/${id}`)
          }
        })

        res.redirect('/')
      }
    })

  }
}

module.exports = ClientController
